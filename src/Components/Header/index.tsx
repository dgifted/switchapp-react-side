import React from 'react';

import styles from './Header.module.css';

type Props = {};

function Header(props: Props): React.FunctionComponentElement<Props> {
    return (
      <header className={styles.headerContainer}>
        <nav className={styles.headerNav}>
          <ul>
            <li>Home</li>
            <li>About</li>
            <li>Contact</li>
            <li>Login</li>
          </ul>
        </nav>
      </header>
    );
}

export default Header;