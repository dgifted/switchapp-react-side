import React from "react";
import { MantineProvider } from "@mantine/core";

type Props = {
  children: React.ReactNode;
};

const AppProviders: React.FC<Props> = ({ children }) => {
  return (
    <React.Fragment>
      <MantineProvider
        theme={{
          fontFamily: "Roboto, sans-serif",
          spacing: { xs: 15, sm: 20, md: 25, lg: 30, xl: 34 },
        }}
      >
        {children}
      </MantineProvider>
    </React.Fragment>
  );
};

export default AppProviders;
